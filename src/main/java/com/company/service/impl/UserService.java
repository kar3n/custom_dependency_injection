package com.company.service.impl;

import com.company.dao.UserRepository;
import com.company.persistence.entity.User;
import com.company.service.Service;


public class UserService implements Service
{
	private UserRepository repository;
	
	public UserService(UserRepository repository)
	{
		this.repository = repository;
	}
	
	@Override
	public User getUSer(User user)
	{
		return repository.getOne(1L);
	}
}
