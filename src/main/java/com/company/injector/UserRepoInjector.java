package com.company.injector;

import com.company.dao.UserRepository;
import com.company.dao.UserRepositoryImpl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class UserRepoInjector implements Injector<UserRepository>
{
	
	@Override
	public UserRepository getInstance(UserRepository inputClass)
	{
		Class<?> clazz = inputClass.getClass();
		try
		{
			Constructor<?> constructor = clazz.getConstructor(inputClass.getClass());
			return (UserRepository) constructor.newInstance(new UserRepositoryImpl());
		}
		catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
