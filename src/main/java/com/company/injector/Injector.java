package com.company.injector;

public interface Injector<T>
{
	T getInstance(T inputClass);
}
