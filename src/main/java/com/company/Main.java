package com.company;

import com.company.annotations.AnnotationProcessingUnit;
import com.company.dao.UserRepository;
import com.company.injector.Injector;
import com.company.injector.UserRepoInjector;

import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException
	{
        Injector<UserRepository> injector = new UserRepoInjector();
        
		AnnotationProcessingUnit.start();
		System.out.println(AnnotationProcessingUnit.getInstancesByType().toString() + " INSTANCES");
		
    }
}
