package com.company.persistence.entity;

import com.company.annotations.Component;

@Component(name = "Entity")
public class User
{
	private long id;
	private String name;
	
	public User()
	{
	}
	
	public User(long id)
	{
		this.id = id;
	}
	
	public User(long id, String name)
	{
		this.id   = id;
		this.name = name;
	}
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		User user = (User) o;
		
		if (getId() != user.getId()) return false;
		return getName() != null ? getName().equals(user.getName()) : user.getName() == null;
	}
	
	@Override
	public int hashCode()
	{
		int result = (int) (getId() ^ (getId() >>> 32));
		result = 31 * result + (getName() != null ? getName().hashCode() : 0);
		return result;
	}
	
	@Override
	public String toString()
	{
		return "User{" + "id=" + id + ", name='" + name + '\'' + '}';
	}
}
