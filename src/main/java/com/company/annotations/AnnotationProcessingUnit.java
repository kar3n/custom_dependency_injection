package com.company.annotations;

import org.reflections.Reflections;
import org.reflections.scanners.*;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class AnnotationProcessingUnit
{
	
	private static final Map<Long, Class<?>> INSTANCE_TYPES = new HashMap<>();
	private static final Set<Object> INSTANCES_BY_TYPE = new HashSet<>();
	private static final String PACKAGE_NAME = "com";
	private static final Reflections REFLECTIONS = new Reflections(new ConfigurationBuilder().setUrls(ClasspathHelper.forPackage(PACKAGE_NAME))
																							 .setScanners(new MethodAnnotationsScanner(), new TypeAnnotationsScanner(), new SubTypesScanner(false), new MemberUsageScanner()));
	
	private static long instanceCounter = 0;
	
	
	public static Map<Long, Class<?>> getInstanceTypes()
	{
		return INSTANCE_TYPES;
	}
	
	public static Set<Object> getInstancesByType()
	{
		return INSTANCES_BY_TYPE;
	}
	
	public static void start() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException
	{
		createClassInstances();
	}
	
	private void checkObject(Object object) throws Exception
	{
		if (Objects.isNull(object))
		{
			throw new NullPointerException();
		}
		
		Class<?> clazz = object.getClass();
		if (!clazz.isAnnotationPresent(Component.class))
		{
			throw new Exception("To be changed"); //TODO change
		}
	}
	
	
	private static void createInterfaceInstances() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
	{
		for (Constructor constructor : getConstructorsAnnotatedWithParticular(Autowired.class))
		{
			Class<?>[] interfaces = constructor.getParameterTypes();
			Set<Class<?>> implementations = new HashSet<>();
			for (Class<?> clazz : interfaces)
			{
				implementations.addAll(REFLECTIONS.getSubTypesOf(clazz));
			}
			for (Class<?> implementation : implementations)
			{
				for (Constructor<?> implConstructor : implementation.getConstructors())
				{
					if (implConstructor.getParameterCount() == 0)
					{
						INSTANCES_BY_TYPE.add(implementation.getConstructor().newInstance());
					}
					else
					{
						createInstancesForInterfaces(implConstructor.getParameterTypes());
					}
				}
			}
		}
	}
	
	private static void createInstancesForInterfaces(Class<?>[] constructorsWithParams) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
	{
		final Set<Class<?>> implementations = new HashSet<>();
		for (Class<?> clazz : constructorsWithParams)
		{
			implementations.addAll(REFLECTIONS.getSubTypesOf(clazz));
		}
		for (Class<?> impl : implementations)
		{
			INSTANCES_BY_TYPE.add(impl.getConstructor().newInstance());
		}
		
	}
	
	private static void createClassInstances() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException
	{
		for (Class<?> annotatedClass : getTypesAnnotatedWithParticular(Component.class)) //TODO injection
		{
			for (Constructor<?> constructor : annotatedClass.getConstructors())
			{
				if (constructor.getParameterCount() == 0)
				{
					INSTANCES_BY_TYPE.add(constructor.newInstance());
				}
				else
				{
					createInterfaceInstances();
				}
			}
			
		}
	}
	
	static Set<Class<?>> getClasses(String packageName)
	{
		List<ClassLoader> classLoadersList = new LinkedList<>();
		classLoadersList.add(ClasspathHelper.contextClassLoader());
		classLoadersList.add(ClasspathHelper.staticClassLoader());
		
		Reflections reflections = new Reflections(new ConfigurationBuilder().setScanners(new SubTypesScanner(false), new ResourcesScanner())
																			.setUrls(ClasspathHelper.forClassLoader(classLoadersList
																															.toArray(new ClassLoader[0])))
																			.filterInputsBy(new FilterBuilder()
																									.include(FilterBuilder.prefix(packageName))));
		
		Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
		return classes;
	}
	
	
	private static Set<Class<?>> getTypesAnnotatedWithParticular(Class<? extends Annotation> annotatedClass)
	{
		return REFLECTIONS.getTypesAnnotatedWith(annotatedClass);
	}
	
	private static Set<Constructor> getConstructorsAnnotatedWithParticular(Class<? extends Annotation> annotatedConstructor)
	{
		return REFLECTIONS.getConstructorsAnnotatedWith(annotatedConstructor);
	}
	
	private static Set<Method> getMethodsAnnotatedWithParticular(Class<? extends Annotation> annotatedMethod)
	{
		return REFLECTIONS.getMethodsAnnotatedWith(annotatedMethod);
	}
	
	private static Set<Constructor> getConstructorsMatchParticularParams(Class<?>... constructorParams)
	{
		return REFLECTIONS.getConstructorsMatchParams(constructorParams);
	}
	
}