package com.company.dao;

public interface Repository<T, ID>
{
	T getOne(ID id);
}
