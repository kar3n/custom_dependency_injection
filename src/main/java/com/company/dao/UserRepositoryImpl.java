package com.company.dao;

import com.company.annotations.Component;
import com.company.persistence.entity.User;

@Component
public class UserRepositoryImpl implements UserRepository
{
	@Override
	public User getOne(Long id)
	{
		return new User(id);
	}
}
