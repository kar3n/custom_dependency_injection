package com.company.dao;

import com.company.persistence.entity.User;

public interface UserRepository extends Repository<User, Long>
{
}
