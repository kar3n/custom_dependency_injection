package com.company.controller;

import com.company.annotations.Autowired;
import com.company.annotations.Component;
import com.company.injector.Injector;
import com.company.persistence.entity.User;
import com.company.service.Service;

@Component
public class MainController
{
	private Service service;
	private Injector injector;
	
	@Autowired
	public MainController(Service service)
	{
		this.service = service;
//		this.injector = injector;
	}
	
	public void runProgram()
	{
		service.getUSer(new User(1, "Karen"));
	}
}
